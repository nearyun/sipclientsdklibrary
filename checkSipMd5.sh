#!/bin/bash
src_path="/Users/xiaohua/Documents/workspace/SipRemoteActivity/libs"
src_armeabi=${src_path}"/armeabi/libsipclient-jni.so"
src_v7a=${src_path}"/armeabi-v7a/libsipclient-jni.so"
src_arm64_v8a=${src_path}"/arm64-v8a/libsipclient-jni.so"
src_mips=${src_path}"/mips/libsipclient-jni.so"
src_x86=${src_path}"/x86/libsipclient-jni.so"
src_x86_64=${src_path}"/x86_64/libsipclient-jni.so"

dst_path="/Users/xiaohua/Documents/workspace/SipClientSDKLibrary/libs"
dst_armeabi=${dst_path}"/armeabi/libsipclient-jni.so"
dst_v7a=${dst_path}"/armeabi-v7a/libsipclient-jni.so"
dst_arm64_v8a=${dst_path}"/arm64-v8a/libsipclient-jni.so"
dst_mips=${dst_path}"/mips/libsipclient-jni.so"
dst_x86=${dst_path}"/x86/libsipclient-jni.so"
dst_x86_64=${dst_path}"/x86_64/libsipclient-jni.so"


echo "md5  armeabi ---------------------"
md5 ${src_armeabi}
md5 ${dst_armeabi}
echo ""
echo "md5  v7a ---------------------"
md5 ${src_v7a}
md5 ${dst_v7a}

echo ""
echo "md5  arm64_v8a ---------------------"
md5 ${src_arm64_v8a}
md5 ${dst_arm64_v8a}

echo ""
echo "md5  mips ---------------------"
md5 ${src_mips}
md5 ${dst_mips}

echo ""
echo "md5  x86 ---------------------"
md5 ${src_x86}
md5 ${dst_x86}

echo ""
echo "md5  x86_64 ---------------------"
md5 ${src_x86_64}
md5 ${dst_x86_64}

