package com.nearyun.sip.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.nearyun.sip.io.SipSharePerference;
import com.nearyun.sip.model.SipAccount;
import com.nearyun.sip.model.SipPhoneService;
import com.nearyun.sip.util.SipLog;

public class AutoBootBroadcastReceiver extends BroadcastReceiver {
	private static final String TAG = AutoBootBroadcastReceiver.class.getSimpleName();

	@Override
	public void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		if (!TextUtils.equals(action, Intent.ACTION_BOOT_COMPLETED))
			return;

		SipAccount account = SipSharePerference.readSipAccount(context);
		if (account == null) {
			SipLog.w(TAG, "account is null");
			return;
		}
		SipPhoneService mSipPhoneService = new SipPhoneService(context);
		mSipPhoneService.doStartService(account);
	}

}
