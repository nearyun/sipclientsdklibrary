package com.nearyun.sip.receiver;

import android.bluetooth.BluetoothA2dp;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.text.TextUtils;

import com.nearyun.sip.listener.OnAudioChannelChangedListener;
import com.nearyun.sip.util.SipLog;

public class AudioChannelChangeReceiver extends BroadcastReceiver implements OnAudioChannelChangedListener {
	private static final String TAG = AudioChannelChangeReceiver.class.getSimpleName();

	private boolean hasBluetoothConnected = false;
	private boolean hasHeadSet = false;
	private boolean useEarphone = false;

	protected Context mContext = null;
	protected AudioManager audioManager;

	private final PowerManager pm;
	private PowerManager.WakeLock wl;
	public static final int PROXIMITY_SCREEN_OFF_WAKE_LOCK = 32;

	public AudioChannelChangeReceiver(Context context) {
		mContext = context;

		audioManager = (AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE);
		pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		try {
			wl = pm.newWakeLock(PROXIMITY_SCREEN_OFF_WAKE_LOCK, "SpeakerProximity");
		} catch (Exception e) {
			wl = null;
			SipLog.printf("can't get wakelock to turn display off, sorry");
		}
	}

	/**
	 * 获取蓝牙是否已连接
	 * 
	 * @return
	 */
	private boolean getBluetoothConnection() {
		if (BluetoothAdapter.getDefaultAdapter() == null)
			return false;
		int btHeadsetFlag = BluetoothAdapter.getDefaultAdapter().getProfileConnectionState(BluetoothProfile.HEADSET);
		return BluetoothProfile.STATE_CONNECTED == btHeadsetFlag;
		// int btA2dpFlag =
		// BluetoothAdapter.getDefaultAdapter().getProfileConnectionState(BluetoothProfile.A2DP);
		// return BluetoothProfile.STATE_CONNECTED == btA2dpFlag;
	}

	private void release() {
		if (wl != null) {
			if (wl.isHeld()) {
				wl.release();
			}
		}
	}

	private void acquire() {
		if (wl != null) {
			if (!wl.isHeld()) {
				wl.acquire();
			}
		}
	}

	/**
	 * 设置当前手机使用的频道
	 * 
	 * @param useSpeaker
	 *            是否使用扬声器
	 * @param isBluetoothConnected
	 *            是否蓝牙已连接
	 */
	protected void setSoundChannelMode(boolean useSpeaker, boolean isBluetoothConnected) {
		audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
		if (!isBluetoothConnected && audioManager.isBluetoothScoOn()) {
			audioManager.stopBluetoothSco();
			audioManager.setBluetoothScoOn(false);
		}

		if (useSpeaker) {
			audioManager.setBluetoothScoOn(false);
			audioManager.setSpeakerphoneOn(true);
			release();
			onSpeaker();
		} else {
			if (isBluetoothConnected) {
				release();
				try {
					audioManager.startBluetoothSco();
					audioManager.setBluetoothScoOn(true);
					onBluetoothA2dp();
				} catch (Exception e) {
					e.printStackTrace();
					audioManager.stopBluetoothSco();
					audioManager.setBluetoothScoOn(false);
					setSoundChannelMode(true, false);
				}
			} else {
				audioManager.setBluetoothScoOn(false);
				audioManager.setSpeakerphoneOn(false);
				if (hasHeadSet) {
					release();
					onHeadset();
				} else {
					acquire();
					onEarPhone();
				}
			}
		}

	}

	/**
	 * 重置模式
	 */
	private void resetChannelMode() {
		audioManager.setMode(AudioManager.MODE_NORMAL);
		audioManager.stopBluetoothSco();
		audioManager.setBluetoothScoOn(false);
		audioManager.setSpeakerphoneOn(false);
		release();
	}

	private void notifySoundDeviceChanged() {
		if (hasHeadSet) {
			SipLog.d(TAG, "使用耳机");
			setSoundChannelMode(false, false);
		} else if (hasBluetoothConnected) {
			SipLog.d(TAG, "使用蓝牙");
			setSoundChannelMode(false, true);
		} else if (useEarphone) {
			SipLog.d(TAG, "使用听筒");
			setSoundChannelMode(false, false);
		} else {
			SipLog.d(TAG, "使用扬声器");
			setSoundChannelMode(true, false);
		}
	}

	public void useSpeaker(boolean flag) {
		useEarphone = !flag;
		notifySoundDeviceChanged();
	}

	public boolean isUseSpeaker() {
		return !useEarphone;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		mHandle.removeCallbacks(initialRunnable);
		String action = intent.getAction();
		if (TextUtils.equals(action, Intent.ACTION_HEADSET_PLUG)) {
			handleHeadsetPlug(context, intent);
		} else if (TextUtils.equals(action, BluetoothA2dp.ACTION_CONNECTION_STATE_CHANGED)) {
			handleBluetoothA2dpConnection(context, intent);
		}
	}

	private void handleBluetoothA2dpConnection(Context context, Intent intent) {
		Bundle bundle = intent.getExtras();
		int state = bundle.getInt(BluetoothA2dp.EXTRA_STATE);
		switch (state) {
		case BluetoothA2dp.STATE_DISCONNECTING:
			SipLog.d(TAG, "A2dp 正在断开");
			hasBluetoothConnected = getBluetoothConnection();
			notifySoundDeviceChanged();
			break;
		case BluetoothA2dp.STATE_DISCONNECTED:
			SipLog.d(TAG, "A2dp 断开");
			hasBluetoothConnected = getBluetoothConnection();
			notifySoundDeviceChanged();
			break;
		case BluetoothA2dp.STATE_CONNECTING:
			SipLog.d(TAG, "A2dp 正在连接");
			break;
		case BluetoothA2dp.STATE_CONNECTED:
			SipLog.d(TAG, "A2dp 已连接");
			hasBluetoothConnected = getBluetoothConnection();
			notifySoundDeviceChanged();
			break;
		default:
			SipLog.w(TAG, "A2dp 未知 " + state);
			break;
		}
	}

	private void handleHeadsetPlug(Context context, Intent intent) {
		if (!intent.hasExtra("state"))
			return;
		Bundle bundle = intent.getExtras();
		// String name = intent.getStringExtra("name");
		// int microphone = intent.getIntExtra("microphone", -1);
		int state = intent.getIntExtra("state", 2);
		switch (state) {
		case 0:// 拔出
			SipLog.d(TAG, "耳机拔出：" + bundle.toString());
			hasHeadSet = false;
			notifySoundDeviceChanged();
			break;
		case 1:// 插入
			SipLog.d(TAG, "耳机插入：" + bundle.toString());
			hasHeadSet = true;
			notifySoundDeviceChanged();
			break;
		default:
			break;
		}
	}

	private final IntentFilter getDeviceIntentFilter() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(Intent.ACTION_HEADSET_PLUG);
		filter.addAction(BluetoothA2dp.ACTION_CONNECTION_STATE_CHANGED);
		return filter;
	}

	private Handler mHandle = new Handler();
	private final int DELAY_TIME = 600;
	private Runnable initialRunnable = new Runnable() {
		public void run() {
			notifySoundDeviceChanged();
		}
	};

	/**
	 * 注册广播监听耳机、蓝牙的连接状态
	 */
	@SuppressWarnings("deprecation")
	public void registerBroadcast() {
		useEarphone = false;
		hasBluetoothConnected = getBluetoothConnection();
		hasHeadSet = audioManager.isWiredHeadsetOn();
		SipLog.i(TAG, "registerBroadcast bluetooth connected:" + hasBluetoothConnected + " headset:" + hasHeadSet);

		mContext.registerReceiver(this, getDeviceIntentFilter());

		mHandle.postDelayed(initialRunnable, DELAY_TIME);
	}

	public boolean hasBluetoothConnected() {
		return hasBluetoothConnected;
	}

	public boolean hasHeadSet() {
		return hasHeadSet;
	}

	public boolean hasEarPhone() {
		return useEarphone;
	}

	/**
	 * 注销监听耳机、蓝牙连接状态的广播
	 */
	public void unregisterBroadcast() {
		mContext.unregisterReceiver(this);
		resetChannelMode();
	}

	@Override
	public void onBluetoothA2dp() {
		if (mOnAudioChannelChangedListener != null)
			mOnAudioChannelChangedListener.onBluetoothA2dp();
	}

	@Override
	public void onHeadset() {
		if (mOnAudioChannelChangedListener != null)
			mOnAudioChannelChangedListener.onHeadset();
	}

	@Override
	public void onSpeaker() {
		if (mOnAudioChannelChangedListener != null)
			mOnAudioChannelChangedListener.onSpeaker();
	}

	@Override
	public void onEarPhone() {
		if (mOnAudioChannelChangedListener != null)
			mOnAudioChannelChangedListener.onEarPhone();
	}

	private OnAudioChannelChangedListener mOnAudioChannelChangedListener;

	public void setOnAudioChannelChangedListener(OnAudioChannelChangedListener listener) {
		mOnAudioChannelChangedListener = listener;
	}

}
