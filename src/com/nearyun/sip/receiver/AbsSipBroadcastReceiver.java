package com.nearyun.sip.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.nearyun.sip.model.SipPhoneIntentFilter;
import com.nearyun.sip.util.SipLog;

public abstract class AbsSipBroadcastReceiver extends BroadcastReceiver {
	private static final String TAG = AbsSipBroadcastReceiver.class.getSimpleName();

	@Override
	public final void onReceive(Context context, Intent intent) {
		String action = intent.getAction();
		int index = -1;
		switch (action) {
		case SipPhoneIntentFilter.ACTION_SIP_EVENT_REGISTERED:
			onSipRegistered(context);
			break;
		case SipPhoneIntentFilter.ACTION_SIP_EVENT_UNREGISTERED:
			onSipUnregistered(context);
			break;
		case SipPhoneIntentFilter.ACTION_SIP_EVENT_ON_INVITE:
			index = intent.getIntExtra(SipPhoneIntentFilter.EXTRA_INDEX, -1);
			String callerUri = intent.getStringExtra(SipPhoneIntentFilter.EXTRA_ON_INVITE_STRING_CALLER_URI);
			String callerId = intent.getStringExtra(SipPhoneIntentFilter.EXTRA_ON_INVITE_STRING_CALLER_ID);
			String callerName = intent.getStringExtra(SipPhoneIntentFilter.EXTRA_ON_INVITE_STRING_CALLER_NAME);
			onSipInvite(context, index, callerUri, callerId, callerName);
			break;
		case SipPhoneIntentFilter.ACTION_EVENT_ON_ANSWER:
			index = intent.getIntExtra(SipPhoneIntentFilter.EXTRA_INDEX, -1);
			onSipAnswer(context, index);
			break;
		case SipPhoneIntentFilter.ACTION_EVENT_ON_CONNECTED:
			index = intent.getIntExtra(SipPhoneIntentFilter.EXTRA_INDEX, -1);
			onSipConnected(context, index);
			break;
		case SipPhoneIntentFilter.ACTION_EVENT_ON_DISCONNECTED:
			index = intent.getIntExtra(SipPhoneIntentFilter.EXTRA_INDEX, -1);
			onSipDisconnected(context, index);
			break;
		case SipPhoneIntentFilter.ACTION_EVENT_ON_FAILURE:
			index = intent.getIntExtra(SipPhoneIntentFilter.EXTRA_INDEX, -1);
			int failureStatus = intent.getIntExtra(SipPhoneIntentFilter.EXTRA_ON_FAILURE_INT_STATUS, -1);
			String reason = intent.getStringExtra(SipPhoneIntentFilter.EXTRA_ON_FAILURE_STRING_REASON);
			onSipFailure(context, index, failureStatus, reason);
			break;
		case SipPhoneIntentFilter.ACTION_EVENT_ON_IDLE:
			index = intent.getIntExtra(SipPhoneIntentFilter.EXTRA_INDEX, -1);
			onSipIdle(context, index);
			break;
		case SipPhoneIntentFilter.ACTION_EVENT_ON_PROCEEDING:
			index = intent.getIntExtra(SipPhoneIntentFilter.EXTRA_INDEX, -1);
			int proceedingStatus = intent.getIntExtra(SipPhoneIntentFilter.EXTRA_ON_PROCEEDING_INT_STATUS, -1);
			onSipProceeding(context, index, proceedingStatus);
			break;
		case SipPhoneIntentFilter.ACTION_EVENT_ON_REDIRECTED:
			index = intent.getIntExtra(SipPhoneIntentFilter.EXTRA_INDEX, -1);
			String target = intent.getStringExtra(SipPhoneIntentFilter.EXTRA_ON_REDIRECTED_STRING_TRAGET);
			onSipRedirected(context, index, target);
			break;
		case SipPhoneIntentFilter.ACTION_EVENT_ON_MEDIA_ERROR:
			index = intent.getIntExtra(SipPhoneIntentFilter.EXTRA_INDEX, -1);
			onSipMediaError(context, index);
			break;
		default:
			SipLog.e(TAG, "unknown sip action:" + action);
			break;
		}
	}

	protected abstract void onSipMediaError(Context context, int index);

	protected abstract void onSipRedirected(Context context, int index, String target);

	protected abstract void onSipProceeding(Context context, int index, int proceedingStatus);

	protected abstract void onSipIdle(Context context, int index);

	protected abstract void onSipFailure(Context context, int index, int failureStatus, String reason);

	protected abstract void onSipDisconnected(Context context, int index);

	protected abstract void onSipConnected(Context context, int index);

	protected abstract void onSipAnswer(Context context, int index);

	protected abstract void onSipInvite(Context context, int index, String callerUri, String callerId, String callerName);

	protected abstract void onSipUnregistered(Context context);

	protected abstract void onSipRegistered(Context context);
}
