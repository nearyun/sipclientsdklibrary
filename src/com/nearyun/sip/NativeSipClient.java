/* 
 * cd /home/xiaocui/C++/projects/uswitch/src/sipclient/android
 * javac src/com/sokuda/sip/sipclient.java
 * javah -jni -v -o jni/sipclient_jni.h src/com/sokuda/sip/sipclient.java
 */
package com.nearyun.sip;

public class NativeSipClient {

	public native long create(String dispname, String userinfo, String domain);

	public native void destroy(long client);

	public native int setparam(long client, String pname, String pvalue);

	public native int setsipevents(long client, ISipClientSipEvent cb);

	public native int setcallevents(long client, ISipClientCallEvent cb);

	public native int register(long client, String registrar);

	public native int unregister(long client, String registrar);

	public native int invite(long client, String target);

	/* send 180 */
	public native int ringing(long client);

	/* send 200 OK */
	public native int answer(long client);

	/* Send 486 BUSY */
	public native int busy(long client);

	public native int senddtmf(long client, String dtmf);

	// hangup(client, 486,"busy")
	public native int hangup(long client, int status, String reason);

	/** 1:true; 0:false */
	public native int mute(long client, int muted);

	/* new in ver 1.2 */
	/**
	 * common status are</P> - 480: unavailable, reject the call, do not disturb
	 * </P> - 486: busy now</P>
	 */
	public native int reject(long client, int status);

	/*
	 * re-initialize local mic & speaker for IOS, while PSTN call ended,
	 * recover() can help to record and playback VOIP voice again
	 */
	public native int recover(long client);

	/* return 1 for registered, 0 for not yet registered */
	public native int isregistered(long client);

	/*
	 * enum sipclient_state { SIPCLIENT_NEW = 0, SIPCLIENT_IDLE = 1,
	 * SIPCLIENT_ALERTING = 2, SIPCLIENT_RINING = 3, SIPCLIENT_BUSY = 4,
	 * SIPCLIENT_HANGUP =5 , }; return numeric client state value
	 */
	public native int getstate(long client);

	/*
	 * re-do ICE check, should call after registered and still in call, for now,
	 * sip stack will do that automatically if network error.
	 */
	public native int renegotiate(long client);

	/*
	 * after 1.6, enable or diable aec latency: -1, disable aec >=0, fixed aec
	 * latency, in milliseconds, and also, non-nagitive value will disable auto
	 * calibrate
	 */
	public native int aec(long client, int latency);

	/*
	 * duration: calibrate duration in milliseconds, common value is 700ms,
	 * calibrate(client, 700) will start a new aec calibrate. you can hear a
	 * beep(450hz) lasting for almost 350 ms. duration > 700ms, also acceptable,
	 * but will bother user longer. duration < 200ms, will cancel current
	 * calibrating
	 */
	public native int calibrate(long client, int duration);

	/*
	 * result: get calibrate result or latest aec() setting value, in
	 * milliseconds
	 */
	public native int getlatency(long client);

	/*
	 * 挂断所有呼叫
	 */
	public native int clear(long client, int status, String reason);

	/*
	 * 获取某个呼叫的信令状态
	 */
	public native int getcallstate(long client, int index);

	/* after 1.11 */
	/*
	 * 切换呼叫
	 */
	public native int switchcall(long client, int index);

	public native int current(long client);//currentcall

	/* after 1.13 */

	public native int callRinging(long client, int index);

	public native int callReject(long client, int index, int status);

	public native int callAnswer(long client, int index);

	public native int callHangup(long client, int index, int status, String reason);

	public native int callRenegotiate(long client, int index);
}