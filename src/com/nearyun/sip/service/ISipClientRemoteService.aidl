package com.nearyun.sip.service;

interface ISipClientRemoteService {
    void unregister();
    void register();
    void invite(String target);
    void senddtmf(String dtmf);
    void hangup();
    void ringing();
    void answer();
    void busy();
    void mute(boolean flag);
    boolean isMute();
    void reject(int status);
    void recover();
    boolean isRegistered();
    int getState();
    void destroy();
    void renegotiate();
    boolean aec(int delayms);
    void calibrate(int duration);
   	int getlatency();
   	
   	void switchcall(int index);
   	int currentcall();
   	void clear();
    
    boolean isUseSpeaker();
    void userSpeaker(boolean flag);
    
    void keepalive();
    
    void setsamplerate(boolean useWIFI);
    
    void callRing(int index);
    void callReject(int index, int status);
    void callAnswer(int index);
    void callHangup(int index);
    void callRenegotiate(int index);
}