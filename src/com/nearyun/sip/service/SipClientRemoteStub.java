package com.nearyun.sip.service;

import android.content.Context;
import android.os.Handler;
import android.os.RemoteException;

import com.nearyun.sip.ISipClientCallEvent;
import com.nearyun.sip.ISipClientSipEvent;
import com.nearyun.sip.SipClient;
import com.nearyun.sip.listener.OnAudioChannelChangedListener;
import com.nearyun.sip.model.SipAccount;
import com.nearyun.sip.receiver.AudioChannelChangeReceiver;
import com.nearyun.sip.service.SipServiceHeartbeat.OnHeartbeatTimeoutListener;
import com.nearyun.sip.util.SipLog;
import com.tornado.util.NetworkUtil;

public class SipClientRemoteStub extends ISipClientRemoteService.Stub {

	private static final String TAG = SipClientRemoteStub.class.getSimpleName();

	private SipServiceHeartbeat mSipServiceHeartbeat;

	private SipClient mSipClient = null;

	public boolean hasCreateOne() {
		return mSipClient != null;
	}

	private AudioChannelChangeReceiver mAudioChannelChangeReceiver;

	public void registerAudioChannelChangeBroadcast() {
		mAudioChannelChangeReceiver.registerBroadcast();
	}

	public void unregisterAudioChannelChangeBroadcast() {
		mAudioChannelChangeReceiver.unregisterBroadcast();
	}

	public void startHeatbeat() {
		mSipServiceHeartbeat.setRunning(true);
		mSipServiceHeartbeat.start();
	}

	public void stopHeatbeat() {
		mSipServiceHeartbeat.setRunning(false);
		mSipServiceHeartbeat.stop();
	}

	@Override
	public void keepalive() throws RemoteException {
		mSipServiceHeartbeat.restart();
	}

	@Override
	public boolean isUseSpeaker() throws RemoteException {
		return mAudioChannelChangeReceiver.isUseSpeaker();
	}

	@Override
	public void userSpeaker(boolean flag) throws RemoteException {
		mAudioChannelChangeReceiver.useSpeaker(flag);
	}

	private Context mContext;

	public SipClientRemoteStub(Context context) {
		mContext = context;
		mSipServiceHeartbeat = new SipServiceHeartbeat(new OnHeartbeatTimeoutListener() {

			@Override
			public void onTimeout() {
				SipLog.w(TAG, "heartbeat is timeout");
				try {
					hangup();
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}

		});
		mAudioChannelChangeReceiver = new AudioChannelChangeReceiver(context);
		mAudioChannelChangeReceiver.setOnAudioChannelChangedListener(new OnAudioChannelChangedListener() {

			@Override
			public void onSpeaker() {
				SipLog.d(TAG, "onSpeaker");
				try {
					if (getState() == SipClient.STATE_SIPCLIENT_BUSY) {
						if (aec(0))
							calibrate(700);
						else
							calibrate(0);
					}
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onHeadset() {
				SipLog.d(TAG, "onHeadset");
				try {
					if (getState() == SipClient.STATE_SIPCLIENT_BUSY) {
						aec(-1);
					}
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onBluetoothA2dp() {
				SipLog.d(TAG, "onBluetoothA2dp");
				try {
					if (getState() == SipClient.STATE_SIPCLIENT_BUSY) {
						aec(-1);
					}
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}

			@Override
			public void onEarPhone() {
				SipLog.d(TAG, "onEarPhone");
				try {
					if (getState() == SipClient.STATE_SIPCLIENT_BUSY) {
						if (aec(0))
							calibrate(700);
						else
							calibrate(0);
					}
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void create(SipAccount account, ISipClientSipEvent mSipClientCallEvent, ISipClientCallEvent mSipEvent) {
		mSipClient = new SipClient(account);
		mSipClient.create();
		mSipClient.setcallevents(mSipEvent);
		mSipClient.setsipevents(mSipClientCallEvent);
	}

	@Override
	public void unregister() throws RemoteException {
		SipLog.d(TAG, "invoke unregister");
		if (mSipClient != null)
			mSipClient.unregister();
	}

	private Handler mHandler = new Handler();
	private Runnable registerRunnable = new Runnable() {
		@Override
		public void run() {
			SipLog.d(TAG, "invoke register done");
			if (mSipClient != null) {
				mSipClient.register(getNetworkTypeString());
			}
		}
	};

	private String getNetworkTypeString() {
		int type = NetworkUtil.getCurrentNetworkType(mContext);
		switch (type) {
		case NetworkUtil.NETWORK_CLASS_2G:
			return SipClient.NETWORK_TYPE_2G;
		case NetworkUtil.NETWORK_CLASS_3G:
			return SipClient.NETWORK_TYPE_3G;
		case NetworkUtil.NETWORK_CLASS_4G:
			return SipClient.NETWORK_TYPE_4G;
		case NetworkUtil.NETWORK_CLASS_WIFI:
			return SipClient.NETWORK_TYPE_WIFI;
		default:
			return SipClient.NETWORK_TYPE_NONE;
		}
	}

	public void cancelRegisterTimer() {
		SipLog.d(TAG, "cancel register timer");
		mHandler.removeCallbacks(registerRunnable);
	}

	@Override
	public void register() throws RemoteException {
		SipLog.d(TAG, "invoke delay register");
		mHandler.removeCallbacks(registerRunnable);
		mHandler.postDelayed(registerRunnable, 1000);
	}

	@Override
	public void invite(String target) throws RemoteException {
		SipLog.d(TAG, "invoke invite");
		if (mSipClient != null)
			mSipClient.invite(target);
	}

	@Override
	public void hangup() throws RemoteException {
		SipLog.d(TAG, "invoke hangup");
		if (mSipClient != null)
			mSipClient.hangup();
	}

	@Override
	public void senddtmf(String dtmf) throws RemoteException {
		SipLog.d(TAG, "invoke senddtmf " + dtmf);
		if (mSipClient != null)
			mSipClient.senddtmf(dtmf);
	}

	@Override
	public void setsamplerate(boolean useWIFI) throws RemoteException {
		SipLog.d(TAG, "invoke setsamplerate " + useWIFI);
		if (mSipClient != null)
			mSipClient.setsamplerate(useWIFI);
	}

	@Override
	public void ringing() throws RemoteException {
		SipLog.d(TAG, "invoke ringing");
		if (mSipClient != null)
			mSipClient.ringing();
	}

	@Override
	public void answer() throws RemoteException {
		SipLog.d(TAG, "invoke answer");
		if (mSipClient != null)
			mSipClient.answer();
	}

	@Override
	public void busy() throws RemoteException {
		SipLog.d(TAG, "invoke busy");
		if (mSipClient != null)
			mSipClient.busy();
	}

	@Override
	public void mute(boolean flag) throws RemoteException {
		SipLog.d(TAG, "invoke mute " + flag);
		if (mSipClient != null)
			mSipClient.mute(flag);
	}

	@Override
	public boolean isMute() throws RemoteException {
		SipLog.d(TAG, "invoke isMute");
		if (mSipClient != null) {
			return mSipClient.isMute();
		}
		return false;
	}

	@Override
	public void reject(int status) throws RemoteException {
		SipLog.d(TAG, "invoke reject " + status);
		if (mSipClient != null)
			mSipClient.reject(status);
	}

	@Override
	public void recover() throws RemoteException {
		SipLog.d(TAG, "invoke recover");
		if (mSipClient != null)
			mSipClient.recover();
	}

	@Override
	public boolean isRegistered() throws RemoteException {
		SipLog.d(TAG, "invoke isRegistered");
		if (mSipClient != null)
			return mSipClient.isRegistered();
		return false;
	}

	/**
	 * 返回-1 代表client have not create;
	 * 
	 * 返回-2 代表sipclient 未运行
	 */
	@Override
	public int getState() throws RemoteException {
		SipLog.d(TAG, "invoke getState");
		if (mSipClient != null)
			return mSipClient.getState();
		return -2;
	}

	@Override
	public void destroy() throws RemoteException {
		SipLog.d(TAG, "invoke destroy");
		mHandler.removeCallbacks(registerRunnable);
		if (mSipClient != null) {
			mSipClient.destroy();
			mSipClient = null;
		}
	}

	@Override
	public void renegotiate() throws RemoteException {
		SipLog.d(TAG, "invoke renegotiate");
		if (mSipClient != null) {
			mSipClient.renegotiate();
		}
	}

	/**
	 * -1 代表取消回声抑制 大于等于0的值，都是设置回声抑制多少ms后生效
	 * 
	 * 当delayms为-1时，调用calibrate(0)
	 * 
	 * @return true，需要调用calibrate(700)；false，需要调用calibrate(0)
	 */
	@Override
	public boolean aec(int delayms) throws RemoteException {
		SipLog.d(TAG, "invoke aec " + delayms);
		if (mSipClient != null) {
			SipLog.d(TAG, "invoke aec done");
			// 遇到三星机型，都取消回声抑制
			// String brand =
			// android.os.Build.BRAND.toLowerCase(Locale.getDefault());

			// if (TextUtils.equals(brand, "samsung") || TextUtils.equals(brand,
			// "meizu")
			// || TextUtils.equals(brand, "nubia")) {
			// SipLog.d(TAG, "三星 取消回声抑制");
			// mSipClient.aec(-1);
			// return false;
			// } else {
			SipLog.d(TAG, android.os.Build.BRAND + " 使用回声抑制");
			mSipClient.aec(delayms);
			return !(delayms == -1);
			// }
		}
		return false;
	}

	@Override
	public void calibrate(int duration) throws RemoteException {
		SipLog.d(TAG, "invoke calibrate " + duration);
		if (mSipClient != null) {
			SipLog.d(TAG, "invoke calibrate done");
			mSipClient.calibrate(duration);
		}
	}

	@Override
	public int getlatency() throws RemoteException {
		SipLog.d(TAG, "");
		if (mSipClient != null) {
			return mSipClient.getlatency();
		} else
			return -1;
	}

	@Override
	public void switchcall(int index) throws RemoteException {
		SipLog.d(TAG, "invoke switchcall index:" + index);
		if (mSipClient != null) {
			mSipClient.switchcall(index);
		}
	}

	/**
	 * -1:代表当前没有呼叫;</P>-2:代表代表client未创建;</P>-3:代表SipClient没有在运行
	 * 
	 * @return
	 */
	@Override
	public int currentcall() throws RemoteException {
		SipLog.d(TAG, "invoke currentcall");
		if (mSipClient != null)
			return mSipClient.currentcall();
		else
			return -3;
	}

	@Override
	public void clear() throws RemoteException {
		SipLog.d(TAG, "invoke clear");
		if (mSipClient != null) {
			mSipClient.clear();
		}
	}

	@Override
	public void callRing(int index) throws RemoteException {
		SipLog.d(TAG, "invoke callRing " + index);
		if (mSipClient != null) {
			mSipClient.callRing(index);
		}
	}

	@Override
	public void callReject(int index, int status) throws RemoteException {
		SipLog.d(TAG, "invoke callReject " + index);
		if (mSipClient != null) {
			mSipClient.callReject(index, status);
		}
	}

	@Override
	public void callAnswer(int index) throws RemoteException {
		SipLog.d(TAG, "invoke callAnswer " + index);
		if (mSipClient != null) {
			mSipClient.callAnswer(index);
		}
	}

	@Override
	public void callHangup(int index) throws RemoteException {
		SipLog.d(TAG, "invoke callHangup " + index);
		if (mSipClient != null) {
			mSipClient.callHangup(index);
		}
	}

	@Override
	public void callRenegotiate(int index) throws RemoteException {
		SipLog.d(TAG, "invoke callRenegotiate " + index);
		if (mSipClient != null) {
			mSipClient.callRenegotiate(index);
		}
	}

}
