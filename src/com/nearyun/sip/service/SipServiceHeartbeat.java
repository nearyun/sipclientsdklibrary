package com.nearyun.sip.service;

import java.util.Timer;
import java.util.TimerTask;

public class SipServiceHeartbeat {

	private Timer heartTimer;
	private final int DELAY_TIME = 10000;

	public interface OnHeartbeatTimeoutListener {
		public void onTimeout();
	}

	private OnHeartbeatTimeoutListener mOnHeartbeatTimeoutListener;

	public SipServiceHeartbeat(OnHeartbeatTimeoutListener listener) {
		mOnHeartbeatTimeoutListener = listener;
	}

	public class HeartbeatTask extends TimerTask {

		@Override
		public void run() {
			mOnHeartbeatTimeoutListener.onTimeout();
		}
	}

	private boolean isRunning = false;

	public void setRunning(boolean flag) {
		isRunning = flag;
	}

	public void start() {
		if (isRunning) {
			heartTimer = new Timer();
			heartTimer.schedule(new HeartbeatTask(), DELAY_TIME);
		}
	}

	public void stop() {
		if (heartTimer != null) {
			heartTimer.cancel();
			heartTimer = null;
		}
	}

	public void restart() {
		stop();
		start();
	}
}
