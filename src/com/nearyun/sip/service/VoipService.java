package com.nearyun.sip.service;

import java.io.Serializable;
import java.util.Calendar;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.nearyun.sip.ISipClientCallEvent;
import com.nearyun.sip.ISipClientSipEvent;
import com.nearyun.sip.SipClient;
import com.nearyun.sip.sipservice;
import com.nearyun.sip.io.SipSharePerference;
import com.nearyun.sip.model.SipAccount;
import com.nearyun.sip.model.SipPhoneIntentFilter;
import com.nearyun.sip.util.SipLog;
import com.tornado.util.NetworkUtil;
import com.tornado.util.PhoneUtil;

public class VoipService extends Service {
	private final String TAG = getClass().getSimpleName();
	public static final String KEY_SIP_ACCOUNT = "cnss.saccount";

	public static final void newInstance(Context context, SipAccount account) {
		Intent intent = new Intent(context, VoipService.class);
		intent.putExtra(KEY_SIP_ACCOUNT, account);
		context.startService(intent);
	}

	private TelephonyManager mTelephonyManager;
	private SipClientRemoteStub mSipClientStub = null;

	private Handler mHandler = new Handler();

	private boolean hasInit = false;

	private Service mService = null;

	private static int sip_tcp_port = 5090;

	public static void setSipTcpPort(int port) {
		sip_tcp_port = port;
	}

	private static String logFile = null;

	public static void setSipLogFile(String logfile) {
		logFile = logfile;
	}

	@Override
	public void onCreate() {
		mService = this;
		mSipClientStub = new SipClientRemoteStub(mService);

		sipservice.loadLibrary();
		if (!TextUtils.isEmpty(logFile)) {
			sipservice.setlogfile(logFile);
		}
		sipservice.setloglevel(3);
		sipservice.enabletcp(sip_tcp_port);// 会享使用5090

		SipLog.d(TAG, "invoke sipservice.start(), port:" + sip_tcp_port);
		SipLog.d(TAG, "package name:" + mService.getPackageName());
		SipLog.d(TAG, "log name:" + logFile);
		sipservice.start();

		IntentFilter filter = new IntentFilter();
		filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
		registerReceiver(networkChangedBroadcastReceiver, filter);

		mTelephonyManager = (TelephonyManager) getSystemService(Service.TELEPHONY_SERVICE);
		mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
	}

	private boolean initClient(SipAccount account) {
		if (account == null) {
			SipLog.e(TAG, "sip account is null");
			return false;
		} else {
			// SipLog.w(TAG, account.toJsonString()); // 可隐藏log
		}

		if (!mSipClientStub.hasCreateOne()) {
			mSipClientStub.create(account, mSipEvent, mSipClientCallEvent);
			SipSharePerference.saveSipAccount(mService, account);
			// 增加
			try {
				mSipClientStub.setsamplerate(false);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
		executeRegisterGlobalTimer();

		hasInit = true;
		return true;

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		SipLog.d(TAG, "onStartCommand intent:" + intent + ", flags:" + flags + ", startId:" + startId);
		if (intent == null) {
			SipAccount account = SipSharePerference.readSipAccount(mService);
			if (account != null)
				initClient(account);
			else
				stopSelf(startId);
		} else {
			Serializable dataSerializable = intent.getSerializableExtra(KEY_SIP_ACCOUNT);
			if (dataSerializable != null && dataSerializable instanceof SipAccount) {
				SipAccount mSipAccount = (SipAccount) intent.getSerializableExtra(KEY_SIP_ACCOUNT);
				initClient(mSipAccount);
			}

		}

		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		SipLog.i(TAG, "onBind");
		return mSipClientStub;
	}

	@Override
	public boolean onUnbind(Intent intent) {
		SipLog.i(TAG, "onUnbind");
		return false;
	}

	@Override
	public void onDestroy() {
		SipLog.i(TAG, "onDestroy");

		mTelephonyManager.listen(mPhoneStateListener, PhoneStateListener.LISTEN_NONE);

		try {
			mSipClientStub.destroy();
		} catch (RemoteException e) {
			SipLog.printStackTrace(e);
		}
		SipLog.i(TAG, "invoke sipservice stop()");
		sipservice.stop();

		unregisterReceiver(networkChangedBroadcastReceiver);

		cancelRegisterGlobalTimer();
	}

	private final PhoneStateListener mPhoneStateListener = new PhoneStateListener() {

		private boolean offhook = false;

		@Override
		public void onCallStateChanged(int state, String incomingNumber) {
			super.onCallStateChanged(state, incomingNumber);
			switch (state) {
			case TelephonyManager.CALL_STATE_IDLE:
				SipLog.i(TAG, "state：挂断");
				if (offhook) {
					try {
						SipLog.i(TAG, "执行 recover");
						mSipClientStub.recover();
					} catch (RemoteException e) {
						e.printStackTrace();
					}
					offhook = false;
				}
				break;
			case TelephonyManager.CALL_STATE_OFFHOOK:
				SipLog.i(TAG, "state：接听");
				offhook = true;
				break;
			case TelephonyManager.CALL_STATE_RINGING:
				SipLog.i(TAG, "state：响铃");
				offhook = true;
				break;
			}
		}
	};

	private ISipClientSipEvent mSipEvent = new ISipClientSipEvent() {

		@Override
		public void onRegistered() {
			SipLog.i(TAG, "Rx --> onRegistered");

			mSipClientStub.cancelRegisterTimer();
			cancelRegisterGlobalTimer();
			Intent intent = new Intent();
			intent.setAction(SipPhoneIntentFilter.ACTION_SIP_EVENT_REGISTERED);
			intent.addCategory(mService.getPackageName());
			sendBroadcast(intent);
		}

		@Override
		public void onUnRegistered() {
			SipLog.w(TAG, "Rx --> onUnRegistered");

			executeRegisterGlobalTimer();
			Intent intent = new Intent();
			intent.setAction(SipPhoneIntentFilter.ACTION_SIP_EVENT_UNREGISTERED);
			intent.addCategory(mService.getPackageName());
			sendBroadcast(intent);
		}

		@Override
		public void onInvite(int index, String callerURI, String callerID, String callerName) {
			SipLog.d(TAG, "Rx --> onInvite ==> index:" + index + " + " + callerURI + " + " + callerID + " + "
					+ callerName);

			if (PhoneUtil.isTelPhoneBusy(mService)) {
				try {
					mSipClientStub.reject(SipClient.CODE_REJECT_486);
				} catch (RemoteException e) {
					SipLog.printStackTrace(e);
				}
				return;
			}

			try {
				int networkType = NetworkUtil.getCurrentNetworkType(mService);
				if (NetworkUtil.NETWORK_CLASS_UNAVAILABLE == networkType
						|| NetworkUtil.NETWORK_CLASS_UNKNOWN == networkType
						|| NetworkUtil.NETWORK_CLASS_2G == networkType) {
					SipLog.d(TAG, "current network type is 2G");
					mSipClientStub.reject(SipClient.CODE_REJECT_603);
					return;
				} else {
					mSipClientStub.callRing(index);
				}
			} catch (RemoteException e) {
				SipLog.printStackTrace(e);
				return;
			}
			Intent intent = new Intent();
			intent.setAction(SipPhoneIntentFilter.ACTION_SIP_EVENT_ON_INVITE);
			intent.putExtra(SipPhoneIntentFilter.EXTRA_INDEX, index);
			intent.putExtra(SipPhoneIntentFilter.EXTRA_ON_INVITE_STRING_CALLER_ID, callerID);
			intent.putExtra(SipPhoneIntentFilter.EXTRA_ON_INVITE_STRING_CALLER_URI, callerURI);
			intent.putExtra(SipPhoneIntentFilter.EXTRA_ON_INVITE_STRING_CALLER_NAME, callerName);
			intent.addCategory(mService.getPackageName());
			sendBroadcast(intent);
		}

	};

	private ISipClientCallEvent mSipClientCallEvent = new ISipClientCallEvent() {

		@Override
		public void onRedirected(int index, String target) {
			SipLog.d(TAG, "Rx --> onRedirected ==> index:" + index + " + " + target);
			Intent intent = new Intent();
			intent.setAction(SipPhoneIntentFilter.ACTION_EVENT_ON_REDIRECTED);
			intent.putExtra(SipPhoneIntentFilter.EXTRA_INDEX, index);
			intent.putExtra(SipPhoneIntentFilter.EXTRA_ON_REDIRECTED_STRING_TRAGET, target);
			intent.addCategory(mService.getPackageName());
			sendBroadcast(intent);
		}

		@Override
		public void onProceeding(int index, int status) {
			SipLog.d(TAG, "Rx --> onProceeding ==> index:" + index + " + " + status);

			Intent intent = new Intent();
			intent.setAction(SipPhoneIntentFilter.ACTION_EVENT_ON_PROCEEDING);
			intent.putExtra(SipPhoneIntentFilter.EXTRA_INDEX, index);
			intent.putExtra(SipPhoneIntentFilter.EXTRA_ON_PROCEEDING_INT_STATUS, status);
			intent.addCategory(mService.getPackageName());
			sendBroadcast(intent);
		}

		@Override
		public void onFailure(int index, int status, String reason) {
			SipLog.w(TAG, "Rx --> onFailure ==>  index:" + index + " + " + status + " + " + reason);

			Intent intent = new Intent();
			intent.setAction(SipPhoneIntentFilter.ACTION_EVENT_ON_FAILURE);
			intent.putExtra(SipPhoneIntentFilter.EXTRA_INDEX, index);
			intent.putExtra(SipPhoneIntentFilter.EXTRA_ON_FAILURE_INT_STATUS, status);
			intent.putExtra(SipPhoneIntentFilter.EXTRA_ON_FAILURE_STRING_REASON, reason);
			intent.addCategory(mService.getPackageName());
			sendBroadcast(intent);
		}

		@Override
		public void onDisconnected(int index) {
			SipLog.d(TAG, "Rx --> onDisconnected index:" + index);
			mSipClientStub.unregisterAudioChannelChangeBroadcast();

			Intent intent = new Intent();
			intent.setAction(SipPhoneIntentFilter.ACTION_EVENT_ON_DISCONNECTED);
			intent.putExtra(SipPhoneIntentFilter.EXTRA_INDEX, index);
			intent.addCategory(mService.getPackageName());
			sendBroadcast(intent);

			mSipClientStub.stopHeatbeat();
		}

		@Override
		public void onConnected(int index) {
			SipLog.d(TAG, "Rx --> onConnected index:" + index);
			mSipClientStub.registerAudioChannelChangeBroadcast();

			Intent intent = new Intent();
			intent.setAction(SipPhoneIntentFilter.ACTION_EVENT_ON_CONNECTED);
			intent.putExtra(SipPhoneIntentFilter.EXTRA_INDEX, index);
			intent.addCategory(mService.getPackageName());
			sendBroadcast(intent);

			mSipClientStub.startHeatbeat();
		}

		@Override
		public void onAnswered(int index) {
			SipLog.d(TAG, "Rx --> onAnswered index:" + index);

			Intent intent = new Intent();
			intent.setAction(SipPhoneIntentFilter.ACTION_EVENT_ON_ANSWER);
			intent.putExtra(SipPhoneIntentFilter.EXTRA_INDEX, index);
			intent.addCategory(mService.getPackageName());
			sendBroadcast(intent);
		}

		@Override
		public void onIdle(int index) {
			SipLog.d(TAG, "Rx --> onIdle index:" + index);

			Intent intent = new Intent();
			intent.setAction(SipPhoneIntentFilter.ACTION_EVENT_ON_IDLE);
			intent.putExtra(SipPhoneIntentFilter.EXTRA_INDEX, index);
			intent.addCategory(mService.getPackageName());
			sendBroadcast(intent);

		}

		@Override
		public void onMediaError(int index) {
			SipLog.w(TAG, "Rx --> onMediaError index:" + index);

			try {
				mSipClientStub.renegotiate();
			} catch (RemoteException e) {
				e.printStackTrace();
			}

			Intent intent = new Intent();
			intent.setAction(SipPhoneIntentFilter.ACTION_EVENT_ON_MEDIA_ERROR);
			intent.putExtra(SipPhoneIntentFilter.EXTRA_INDEX, index);
			intent.addCategory(mService.getPackageName());
			sendBroadcast(intent);
		}
	};

	private Runnable changeSampleRateRunnable = new Runnable() {
		@Override
		public void run() {
			try {
				if (mSipClientStub.getState() != SipClient.STATE_SIPCLIENT_BUSY) {
					return;
				}
				int networkType = NetworkUtil.getCurrentNetworkType(mService);
				if (NetworkUtil.NETWORK_CLASS_UNAVAILABLE == networkType
						|| NetworkUtil.NETWORK_CLASS_UNKNOWN == networkType
						|| NetworkUtil.NETWORK_CLASS_2G == networkType || NetworkUtil.NETWORK_CLASS_3G == networkType) {
					mSipClientStub.setsamplerate(false);
				} else {
					mSipClientStub.setsamplerate(true);
				}
				mSipClientStub.renegotiate();
			} catch (RemoteException e) {
				SipLog.printStackTrace(e);
			}
		}
	};

	private BroadcastReceiver networkChangedBroadcastReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (!hasInit)
				return;
			ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
			if (networkInfo != null && networkInfo.isAvailable()) {
				SipLog.d(TAG, "网络状态 on " + networkInfo.getTypeName());
				runRegister();
				mHandler.removeCallbacks(changeSampleRateRunnable);
				mHandler.postDelayed(changeSampleRateRunnable, 2000);
			} else {
				SipLog.d(TAG, "网络状态 off ");
				sipservice.clearconnections();
				mSipClientStub.cancelRegisterTimer();
				cancelRegisterGlobalTimer();
			}
		}

	};

	@SuppressWarnings("unused")
	private boolean isServiceRunning(Context context, String serviceName) {
		ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (serviceName.equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 网络不可用时，不继续注册，并停止计时器；若已注册，也不继续注册，并停止定时器；
	 */
	private boolean runRegister() {
		if (!NetworkUtil.IsNetWorkEnable(mService))
			return false;
		try {
			if (mSipClientStub.isRegistered())
				return false;
			mSipClientStub.register();
			return true;
		} catch (RemoteException e) {
			SipLog.printStackTrace(e);
			return false;
		}
	}

	private Runnable globalTimerRunnable = new Runnable() {

		@Override
		public void run() {
			if (runRegister() && mSipClientStub.hasCreateOne())
				mHandler.postDelayed(globalTimerRunnable, GLOBAL_TIME_INTERVAL);
		}
	};

	private final int GLOBAL_TIME_INTERVAL = 10000;

	private void executeRegisterGlobalTimer() {
		SipLog.d(TAG, "executeRegisterGlobalTimer");
		cancelRegisterGlobalTimer();
		mHandler.post(globalTimerRunnable);
	}

	private final void cancelRegisterGlobalTimer() {
		SipLog.d(TAG, "cancelRegisterGlobalTimer");
		mHandler.removeCallbacks(globalTimerRunnable);
	}

	@Override
	public void onTaskRemoved(Intent rootIntent) {
		SipLog.d(TAG, "onTaskRemoved");
		super.onTaskRemoved(rootIntent);
		reboot(mService);
	}

	private void reboot(Context paramContext) {
		SipAccount account = SipSharePerference.readSipAccount(paramContext);
		if (account == null) {
			SipLog.w(TAG, "account is null");
			return;
		}
		SipLog.i(TAG, "execute reboot");
		Intent intent = new Intent(paramContext, VoipService.class);
		intent.putExtra(KEY_SIP_ACCOUNT, account);
		PendingIntent pendingIntent = PendingIntent.getService(paramContext, 0, intent, 0);

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.add(Calendar.SECOND, 5);

		AlarmManager alarmManager = (AlarmManager) paramContext.getSystemService(Context.ALARM_SERVICE);
		alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
	}
}
