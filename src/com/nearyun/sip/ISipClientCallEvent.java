package com.nearyun.sip;

public interface ISipClientCallEvent {
	/**
	 * 客户端创建完，或电话挂断后，紧跟onDisconnected()
	 */
	void onIdle(int index);

	/**
	 * 响铃事件：180，183
	 * 
	 * 1xx
	 * 
	 * @param status
	 */
	void onProceeding(int index, int status);

	/**
	 * 主叫情况下，遇忙转；呼叫前转（类似http的重定向）
	 * 
	 * 3xx
	 * 
	 * @param target
	 */
	void onRedirected(int index, String target);

	/**
	 * 拒听，遇忙，呼转
	 * 
	 * 3456xx
	 * 
	 * @param status
	 * @param reason
	 */
	void onFailure(int index, int status, String reason);

	/**
	 * 告诉本端，answer()操作已发出
	 * 
	 * 2xx
	 */
	void onAnswered(int index);

	/**
	 * 紧跟onAnswer()
	 * 
	 * 对方回ACK
	 */
	void onConnected(int index);

	/**
	 * 与onConnected()成对出现
	 * 
	 * Bye
	 */
	void onDisconnected(int index);

	/**
	 * 
	 */
	void onMediaError(int index);
}
