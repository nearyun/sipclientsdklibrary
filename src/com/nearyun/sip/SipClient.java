package com.nearyun.sip;

import com.nearyun.sip.model.SipAccount;
import com.nearyun.sip.util.SipLog;

public class SipClient extends NativeSipClient {

	public static final int STATE_SIPCLIENT_NEW = 0;// 初始状态，在穿墙
	public static final int STATE_SIPCLIENT_IDLE = 1;// 穿墙成功
	public static final int STATE_SIPCLIENT_ALERTING = 2;
	public static final int STATE_SIPCLIENT_RINING = 3;
	public static final int STATE_SIPCLIENT_BUSY = 4;// 通话中
	public static final int STATE_SIPCLIENT_HANGUP = 5;

	public static final String NETWORK_TYPE_NONE = "unknown";
	public static final String NETWORK_TYPE_FIXED = "fixed";
	public static final String NETWORK_TYPE_2G = "2g";
	public static final String NETWORK_TYPE_3G = "3g";
	public static final String NETWORK_TYPE_4G = "4g";
	public static final String NETWORK_TYPE_5G = "5g";
	public static final String NETWORK_TYPE_WIFI = "wifi";
	public static final String NETWORK_TYPE_WIFI58 = "wifi58";
	public static final String NETWORK_TYPE_VPN = "vpn";

	public static final String SAMPLERATE_16K = "16000";
	public static final String SAMPLERATE_8K = "8000";

	public static final int CODE_REJECT_603 = 603;// common reject
	public static final int CODE_REJECT_480 = 480;
	public static final int CODE_REJECT_486 = 486;// busy
	public static final int CODE_REJECT_487 = 487;// no response
	public static final int CODE_FAILURE_500 = 500;

	private long client;
	private int muted;

	private SipAccount mAccount;

	public SipClient(SipAccount account) {
		mAccount = account;
	}

	public void setsipevents(ISipClientSipEvent cb) {
		if (client != 0)
			super.setsipevents(client, cb);
	}

	public void setcallevents(ISipClientCallEvent cb) {
		if (client != 0)
			super.setcallevents(client, cb);
	}

	public void finalize() throws Throwable {
		if (client != 0) {
			super.destroy(this.client);
			client = 0;
		}
		super.finalize();
	}

	@SuppressWarnings("unused")
	private void doCreate() {
		this.client = super.create(mAccount.getDisplayname(), mAccount.getUserinfo(), mAccount.getDomain());
		super.setparam(client, "username", mAccount.getUsername());
		super.setparam(client, "password", mAccount.getPassword());
		super.setparam(client, "outbound", "true");
		super.setparam(client, "instanceid", mAccount.getInstanceId());
		super.setparam(client, "iceserver", mAccount.getIceServer());
		// sip:192.168.1.119:5090;transport=TCP;lr
		super.setparam(client, "outboundproxy", mAccount.getProxyServer());
	}

	public boolean create() {
		sipservice.wakeup(this, "doCreate", 2000);
		return client > 0;
	}

	@SuppressWarnings("unused")
	private void doSamplerate() {
		super.setparam(client, "samplerate", samplerate);
	}

	private String samplerate = SAMPLERATE_16K;// 在2G/3G环境下，可以设置为8k，降低带宽占有。默认为16k

	public void setsamplerate(boolean useWIFI) {
		// this.samplerate = useWIFI ? SAMPLERATE_16K : SAMPLERATE_8K;
		this.samplerate = SAMPLERATE_8K;
		if (client != 0)
			sipservice.wakeup(this, "doSamplerate", 2000);
	}

	@SuppressWarnings("unused")
	private void doRegister() {
		super.setparam(client, "network", tmpNetworkType);
		super.register(client, "sip:" + mAccount.getDomain());
	}

	private String tmpNetworkType = NETWORK_TYPE_2G;

	public void register(String networkType) {
		tmpNetworkType = networkType;
		if (client != 0)
			sipservice.wakeup(this, "doRegister", 2000);
	}

	@SuppressWarnings("unused")
	private void doUnregister() {
		super.unregister(client, "sip:" + mAccount.getDomain());
	}

	public void unregister() {
		if (client != 0)
			sipservice.wakeup(this, "doUnregister", 2000);
	}

	@SuppressWarnings("unused")
	private void doInvite() {
		super.invite(client, targetInvited);
	}

	private String targetInvited;
	private String dtmfstr;

	public void invite(String target) {
		targetInvited = target;
		if (client != 0)
			sipservice.wakeup(this, "doInvite", 2000);
	}

	@SuppressWarnings("unused")
	private void doSendDTMF() {
		super.senddtmf(client, dtmfstr);
	}

	public void senddtmf(String dtmf) {
		this.dtmfstr = dtmf;
		if (client != 0)
			sipservice.wakeup(this, "doSendDTMF", 2000);
	}

	@SuppressWarnings("unused")
	private void doHangup() {
		super.hangup(client, 200, "ok");
	}

	public void hangup() {
		if (client != 0)
			sipservice.wakeup(this, "doHangup", 2000);
	}

	@SuppressWarnings("unused")
	private void doDestroy() {
		super.destroy(client);
		client = 0;
	}

	public void destroy() {
		if (client != 0)
			sipservice.wakeup(this, "doDestroy", 2000);
	}

	@SuppressWarnings("unused")
	private void doRinging() {
		super.ringing(client);
	}

	public void ringing() {
		if (client != 0)
			sipservice.wakeup(this, "doRinging", 2000);
	}

	@SuppressWarnings("unused")
	private void doAnswer() {
		super.answer(client);
	}

	public void answer() {
		if (client != 0)
			sipservice.wakeup(this, "doAnswer", 2000);
	}

	@SuppressWarnings("unused")
	private void dobusy() {
		super.busy(client);
	}

	public void busy() {
		if (client != 0)
			sipservice.wakeup(this, "dobusy", 2000);
	}

	@SuppressWarnings("unused")
	private void domute() {
		super.mute(client, muted);
	}

	public void mute(boolean flag) {
		muted = flag ? 1 : 0;
		if (client != 0)
			sipservice.wakeup(this, "domute", 2000);
	}

	public boolean isMute() {
		return 1 == muted;
	}

	private int rejectStatus = CODE_REJECT_603;

	@SuppressWarnings("unused")
	private void doReject() {
		super.reject(client, rejectStatus);
	}

	public void reject(int status) {
		rejectStatus = status;
		if (client != 0)
			sipservice.wakeup(this, "doReject", 2000);
	}

	@SuppressWarnings("unused")
	private void doRecover() {
		super.recover(client);
	}

	public void recover() {
		if (client != 0)
			sipservice.wakeup(this, "doRecover", 2000);
	}

	@SuppressWarnings("unused")
	private void doRenegotiate() {
		super.renegotiate(client);
	}

	public void renegotiate() {
		if (client != 0)
			sipservice.wakeup(this, "doRenegotiate", 2000);
	}

	public boolean isRegistered() {
		if (client != 0) {
			int ret = super.isregistered(client);
			return 1 == ret;
		}
		return false;
	}

	/**
	 * 返回-1 代表client have not create
	 * 
	 * @return
	 */
	public int getState() {
		if (client != 0)
			return super.getstate(client);
		else
			return -1;
	}

	@SuppressWarnings("unused")
	private void doAEC() {
		super.aec(client, delayms);
	}

	private int delayms = -1;

	public void aec(int delay) {
		this.delayms = delay;
		if (client != 0)
			sipservice.wakeup(this, "doAEC", 2000);
	}

	@SuppressWarnings("unused")
	private void doCalibrate() {
		super.calibrate(client, calibrateDuration);
	}

	private int calibrateDuration = 700;

	public void calibrate(int duration) {
		calibrateDuration = duration;
		if (client != 0)
			sipservice.wakeup(this, "doCalibrate", 2000);
	}

	@SuppressWarnings("unused")
	private void doSwitchcall() {
		super.switchcall(client, switchcallIndex);
	}

	private int switchcallIndex;

	public void switchcall(int index) {
		switchcallIndex = index;
		if (client != 0)
			sipservice.wakeup(this, "doSwitchcall", 2000);
	}

	/**
	 * 
	 * @return -1:代表当前没有呼叫;</P>-2:代表代表client未创建
	 */
	public int currentcall() {
		if (client != 0)
			return super.current(client);
		else
			return -2;
	}

	@SuppressWarnings("unused")
	private void doClear() {
		super.clear(client, CODE_REJECT_603, "");
	}

	public void clear() {
		if (client != 0)
			sipservice.wakeup(this, "doClear", 2000);
	}

	public int getlatency() {
		if (client != 0)
			return super.getlatency(client);
		else
			return -1;
	}

	public int getcallstate(int index) {
		if (client != 0)
			return super.getcallstate(client, index);
		else
			return -1;
	}

	private int callRingingIndex = -1;

	@SuppressWarnings("unused")
	private void doCallRinging() {
		super.callRinging(client, callRingingIndex);
	}

	public void callRing(int index) {
		callRingingIndex = index;
		if (client != 0)
			sipservice.wakeup(this, "doCallRinging", 2000);
	}

	private int callRejectIndex = -1;
	private int callRejectStatus = CODE_REJECT_603;

	@SuppressWarnings("unused")
	private void doCallReject() {
		super.callReject(client, callRejectIndex, callRejectStatus);
	}

	public void callReject(int index, int status) {
		callRejectIndex = index;
		callRejectStatus = status;
		if (client != 0)
			sipservice.wakeup(this, "doCallReject", 2000);
	}

	private int callAnswerIndex = -1;

	@SuppressWarnings("unused")
	private void doCallAnswer() {
		super.callAnswer(client, callAnswerIndex);
	}

	public void callAnswer(int index) {
		callAnswerIndex = index;
		if (client != 0)
			sipservice.wakeup(this, "doCallAnswer", 2000);
	}

	private int callHangupIndex = -1;

	@SuppressWarnings("unused")
	private void doCallHangup() {
		super.callHangup(client, callHangupIndex, 200, "ok");
	}

	public void callHangup(int index) {
		callHangupIndex = index;
		if (client != 0)
			sipservice.wakeup(this, "doCallHangup", 2000);
	}

	private int callRenegotiateIndex = -1;

	@SuppressWarnings("unused")
	private void doCallRenegotiate() {
		super.callRenegotiate(client, callRenegotiateIndex);
	}

	public void callRenegotiate(int index) {
		callRenegotiateIndex = index;
		if (client != 0)
			sipservice.wakeup(this, "doCallRenegotiate", 2000);
	}
}
