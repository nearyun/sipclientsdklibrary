package com.nearyun.sip;

public class sipservice {
	static public native int version();

	/**
	 * 0-4 0 debug</p> 1:info</p> 2:trace</p> 3:warn</p> 4:error</p>
	 * 
	 * @param level
	 *            默认1
	 * @return
	 */
	static public native int setloglevel(int level);

	/**
	 * after 1.14
	 * 
	 * @param logpath
	 *            包含文件名
	 * @return
	 */
	static public native int setlogfile(String logpath);

	static public native int sethost(String host);

	static public native int enableudp(int udp_port);

	static public native int enabletcp(int tcp_port);

	static public native int enabletls(int tls_port, String cert_path);

	static public native int start();

	static public native void stop();

	static public native void wakeup(NativeSipClient client, String methodname, int timeout_ms);

	static public native void clearconnections();

	public static void loadLibrary() {
		System.out.println("-=-=-=-= start load library");
		System.loadLibrary("sipclient-jni");
		System.out.println("-=-=-=-= finish load library");
	}
}