package com.nearyun.sip.listener;

public interface OnAudioChannelChangedListener {
	/**
	 * 蓝牙设备
	 */
	public void onBluetoothA2dp();

	/**
	 * 耳机
	 */
	public void onHeadset();

	/**
	 * 扬声器
	 */
	public void onSpeaker();

	/**
	 * 听筒
	 */
	public void onEarPhone();
}
