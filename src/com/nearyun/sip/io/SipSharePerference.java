package com.nearyun.sip.io;

import org.json.JSONObject;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.nearyun.sip.model.SipAccount;
import com.nearyun.sip.util.SipLog;

public class SipSharePerference {

	private static final String FILE_NAME = "sip.io";
	private static final String KEY_ACCOUNT = "KA";

	public static final void saveSipAccount(Context context, SipAccount account) {
		if (account == null)
			return;
		SharedPreferences pref = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
		pref.edit().putString(KEY_ACCOUNT, account.toJsonString()).commit();
	}

	public static final SipAccount readSipAccount(Context context) {
		SharedPreferences pref = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
		SipAccount account = null;
		String accountString = pref.getString(KEY_ACCOUNT, null);
		if (!TextUtils.isEmpty(accountString)) {
			try {
				account = SipAccount.create(new JSONObject(accountString));
			} catch (Exception e) {
				SipLog.printStackTrace(e);
			}
		}
		return account;
	}

	public static final void cleanSipAccount(Context context) {
		SharedPreferences pref = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
		pref.edit().clear().commit();
	}
}
