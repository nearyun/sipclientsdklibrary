package com.nearyun.sip.model;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;

import com.nearyun.sip.service.ISipClientRemoteService;
import com.nearyun.sip.service.VoipService;
import com.nearyun.sip.util.SipLog;

public class SipPhoneService {
	private final String TAG = SipPhoneService.class.getSimpleName();

	public interface OnServiceConnectionListener {
		public void onConnected();

		public void onDisconnected();
	}

	private Context mContext;
	private ISipClientRemoteService sipRemote = null;
	private CounterServiceConnection mServiceConnection = null;

	private OnServiceConnectionListener mOnServiceConnectionListener;

	public SipPhoneService(Context context) {
		mContext = context;
	}

	public void setOnServiceConnectionListener(OnServiceConnectionListener l) {
		mOnServiceConnectionListener = l;
	}

	public void doUnbindService() {
		if (mServiceConnection != null) {
			mContext.unbindService(mServiceConnection);
			mServiceConnection = null;
		}
	}

	public void doBindService() {
		if (mServiceConnection == null) {
			mServiceConnection = new CounterServiceConnection();
			Intent intent = new Intent(mContext, VoipService.class);
			mContext.bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
		}
	}

	public void doStartService(SipAccount account) {
		VoipService.newInstance(mContext, account);
	}

	public void doStopService() {
		Intent intent = new Intent(mContext, VoipService.class);
		mContext.stopService(intent);
	}

	public void useSpeaker(boolean flag) {
		try {
			if (sipRemote != null)
				sipRemote.userSpeaker(flag);
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in useSpeaker " + e.getMessage());
		}
	}

	public boolean isUseSpeaker() {
		try {
			if (sipRemote != null)
				return sipRemote.isUseSpeaker();
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in isUseSpeaker " + e.getMessage());
		}
		return false;
	}

	public void keepalive() {
		try {
			if (sipRemote != null)
				sipRemote.keepalive();
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in keepalive " + e.getMessage());
		}
	}

	private class CounterServiceConnection implements ServiceConnection {
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			SipLog.i(TAG, "onServiceConnected");
			sipRemote = ISipClientRemoteService.Stub.asInterface(service);
			if (mOnServiceConnectionListener != null)
				mOnServiceConnectionListener.onConnected();
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			SipLog.w(TAG, "onServiceDisconnected");
			sipRemote = null;
			if (mOnServiceConnectionListener != null)
				mOnServiceConnectionListener.onDisconnected();
		}
	}

	@SuppressWarnings("unused")
	private void ringing() {
		try {
			if (sipRemote != null)
				sipRemote.ringing();
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in ringing " + e.getMessage());
		}
	}

	public void unregister() {
		try {
			if (sipRemote != null)
				sipRemote.unregister();
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in unregister " + e.getMessage());
		}
	}

	public void register() {
		try {
			if (sipRemote != null)
				sipRemote.register();
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in register " + e.getMessage());
		}
	}

	public void invite(String target) {
		try {
			if (sipRemote != null)
				sipRemote.invite(target);
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in invite " + e.getMessage());
		}
	}

	public void answer() {
		try {
			if (sipRemote != null)
				sipRemote.answer();
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in answer" + e.getMessage());
		}
	}

	public void busy() {
		try {
			if (sipRemote != null)
				sipRemote.busy();
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in busy " + e.getMessage());
		}
	}

	public void hangup() {
		try {
			if (sipRemote != null)
				sipRemote.hangup();
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in hangup " + e.getMessage());
		}
	}

	public void mute(boolean flag) {
		try {
			if (sipRemote != null)
				sipRemote.mute(flag);
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in mute " + e.getMessage());
		}
	}

	public boolean isMute() {
		try {
			if (sipRemote != null) {
				return sipRemote.isMute();
			}
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in isMute " + e.getMessage());
		}
		return false;
	}

	public boolean isRegistered() {
		try {
			if (sipRemote != null)
				return sipRemote.isRegistered();
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in isRegistered " + e.getMessage());
		}
		return false;
	}

	public void reject(int status) {
		try {
			if (sipRemote != null)
				sipRemote.reject(status);
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in reject " + e.getMessage());
		}
	}

	public void recover() {
		try {
			if (sipRemote != null)
				sipRemote.recover();
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in recover " + e.getMessage());
		}
	}

	/**
	 * 返回-1 代表client have not create;
	 * 
	 * 返回-2 代表sipclient 未运行
	 * 
	 * 返回-3 代表sipRemote 为null值
	 * 
	 * @return
	 */
	public int getState() {
		try {
			if (sipRemote != null)
				return sipRemote.getState();
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in getState " + e.getMessage());
		}
		return -3;
	}

	public void switchcall(int index) {
		try {
			if (sipRemote != null)
				sipRemote.switchcall(index);
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in switchcall " + e.getMessage());
		}
	}

	/**
	 * -1:代表当前没有呼叫;</P>-2:代表代表client未创建;</P>
	 * 
	 * -3:代表SipClient没有在运行</p>-4:代表SipRemote未空
	 * 
	 * @return
	 */
	public int currentcall() {
		try {
			if (sipRemote != null)
				return sipRemote.currentcall();
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in currentcall " + e.getMessage());
		}
		return -4;
	}

	public void clear() {
		try {
			if (sipRemote != null)
				sipRemote.clear();
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in clear " + e.getMessage());
		}
	}

	public void callRing(int index) {
		try {
			if (sipRemote != null)
				sipRemote.callRing(index);
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in callRing " + e.getMessage());
		}
	}

	public void callReject(int index, int status) {
		try {
			if (sipRemote != null)
				sipRemote.callReject(index, status);
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in callReject " + e.getMessage());
		}
	}

	public void callAnswer(int index) {
		try {
			if (sipRemote != null)
				sipRemote.callAnswer(index);
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in callAnswer " + e.getMessage());
		}
	}

	public void callHangup(int index) {
		try {
			if (sipRemote != null)
				sipRemote.callHangup(index);
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in callHangup " + e.getMessage());
		}
	}

	public void callRenegotiate(int index) {
		try {
			if (sipRemote != null)
				sipRemote.callRenegotiate(index);
		} catch (RemoteException e) {
			SipLog.e(TAG, "Dead object in callRenegotiate " + e.getMessage());
		}
	}

}
