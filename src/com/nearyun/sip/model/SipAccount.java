package com.nearyun.sip.model;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import android.text.TextUtils;

public class SipAccount implements Serializable {

	private static final long serialVersionUID = -7785117060052484683L;
	/**
	 * 昵称，如：小明
	 */
	private String displayname;
	/**
	 * sip号码，如：12345678
	 */
	private String userinfo;
	/**
	 * 域名，如：micyun.com
	 */
	private String domain;
	/**
	 * 服务器地址，如：192.168.1.119
	 */
	private String proxyServer;
	/**
	 * sip号码，如：12345678
	 */
	private String username;
	/**
	 * sip密码，如：123456
	 */
	private String password;
	/**
	 * 唯一标识
	 */
	private String instanceId;

	/**
	 * 媒体服务器
	 */
	private String iceserver;

	public static final SipAccount create(JSONObject json) throws JSONException, NullPointerException {
		String displayname = json.getString(KEY_DISPLAY_NAME);
		String userinfo = json.getString(KEY_USER_INFO);
		String domain = json.getString(KEY_DOMAIN);
		String proxyServer = json.getString(KEY_PROXY_SERVER);
		String username = json.getString(KEY_USER_NAME);
		String password = json.getString(KEY_PASSWORD);
		String instanceId = json.getString(KEY_INSTANCEID);
		String iceserver = json.getString(KEY_ICESERVER);
		return new SipAccount(displayname, userinfo, domain, proxyServer, username, password, instanceId, iceserver);
	}

	public SipAccount(String displayname, String userinfo, String domain, String proxyServer, String username,
			String password, String instanceId, String iceserver) {
		this.displayname = displayname;
		this.userinfo = userinfo;

		if (TextUtils.isEmpty(domain))
			throw new NullPointerException("error:domain can not be null");
		this.domain = domain;

		if (TextUtils.isEmpty(proxyServer))
			throw new NullPointerException("error:server can not be null");
		this.proxyServer = proxyServer;

		if (TextUtils.isEmpty(username))
			throw new NullPointerException("error:username can not be null");
		this.username = username;

		if (TextUtils.isEmpty(password))
			throw new NullPointerException("error:password can not be null");
		this.password = password;

		if (TextUtils.isEmpty(instanceId))
			throw new NullPointerException("error:instanceId can not be null");
		this.instanceId = instanceId;

		if (TextUtils.isEmpty(iceserver))
			throw new NullPointerException("iceserver can not be null");
		this.iceserver = iceserver;

	}

	private static final String KEY_DISPLAY_NAME = "displayname";
	private static final String KEY_USER_INFO = "userinfo";
	private static final String KEY_DOMAIN = "domain";
	private static final String KEY_PROXY_SERVER = "proxyServer";
	private static final String KEY_USER_NAME = "username";
	private static final String KEY_PASSWORD = "password";
	private static final String KEY_INSTANCEID = "instanceId";
	private static final String KEY_ICESERVER = "iceserver";

	public String toJsonString() {
		try {
			JSONObject json = new JSONObject();
			json.put(KEY_DISPLAY_NAME, displayname);
			json.put(KEY_USER_INFO, userinfo);
			json.put(KEY_DOMAIN, domain);
			json.put(KEY_PROXY_SERVER, proxyServer);
			json.put(KEY_USER_NAME, username);
			json.put(KEY_PASSWORD, password);
			json.put(KEY_INSTANCEID, instanceId);
			json.put(KEY_ICESERVER, iceserver);
			return json.toString();
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getDisplayname() {
		return displayname;
	}

	public String getUserinfo() {
		return userinfo;
	}

	public String getDomain() {
		return domain;
	}

	public String getProxyServer() {
		return proxyServer;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

	public String getInstanceId() {
		return instanceId;
	}

	public String getIceServer() {
		return iceserver;
	}

}
