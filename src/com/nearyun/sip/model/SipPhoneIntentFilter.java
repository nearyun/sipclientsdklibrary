package com.nearyun.sip.model;

import android.content.Context;
import android.content.IntentFilter;

public class SipPhoneIntentFilter {

	private static final String PACKAGE_NAME = "com.nearyun.sip.model.intentfilter.";


	public static final String ACTION_SIP_EVENT_UNREGISTERED = PACKAGE_NAME + "ACTION_SIP_EVENT_UNREGISTERED";
	public static final String ACTION_SIP_EVENT_REGISTERED = PACKAGE_NAME + "ACTION_SIP_EVENT_REGISTERED";
	public static final String ACTION_SIP_EVENT_ON_INVITE = PACKAGE_NAME + "ACTION_SIP_EVENT_ON_INVITE";

	public static final String ACTION_EVENT_ON_FAILURE = PACKAGE_NAME + "ACTION_EVENT_ON_FAILURE";
	public static final String ACTION_EVENT_ON_CONNECTED = PACKAGE_NAME + "ACTION_EVENT_ON_CONNECTED";
	public static final String ACTION_EVENT_ON_DISCONNECTED = PACKAGE_NAME + "ACTION_EVENT_ON_DISCONNECTED";
	public static final String ACTION_EVENT_ON_IDLE = PACKAGE_NAME + "ACTION_EVENT_ON_IDLE";
	public static final String ACTION_EVENT_ON_ANSWER = PACKAGE_NAME + "ACTION_EVENT_ON_ANSWER";
	public static final String ACTION_EVENT_ON_PROCEEDING = PACKAGE_NAME + "ACTION_EVENT_ON_PROCEEDING";
	public static final String ACTION_EVENT_ON_REDIRECTED = PACKAGE_NAME + "ACTION_EVENT_ON_REDIRECTED";
	public static final String ACTION_EVENT_ON_MEDIA_ERROR = PACKAGE_NAME + "ACTION_EVENT_ON_MEDIA_ERROR";

	public static final String EXTRA_INDEX = "EXTRA_INDEX";
	public static final String EXTRA_ON_REDIRECTED_STRING_TRAGET = "EXTRA_ON_REDIRECTED_STRING_TRAGET";
	public static final String EXTRA_ON_PROCEEDING_INT_STATUS = "EXTRA_ON_PROCEEDING_INT_STATUS";
	public static final String EXTRA_ON_FAILURE_INT_STATUS = "EXTRA_ON_FAILURE_INT_STATUS";
	public static final String EXTRA_ON_FAILURE_STRING_REASON = "EXTRA_ON_FAILURE_STRING_REASON";
	public static final String EXTRA_ON_INVITE_STRING_CALLER_URI = "EXTRA_ON_INVITE_STRING_CALLER_URI";
	public static final String EXTRA_ON_INVITE_STRING_CALLER_ID = "EXTRA_ON_INVITE_STRING_CALLER_ID";
	public static final String EXTRA_ON_INVITE_STRING_CALLER_NAME = "EXTRA_ON_INVITE_STRING_CALLER_NAME";

	public static final IntentFilter getFilter(Context context) {
		IntentFilter filter = new IntentFilter();
		filter.addAction(ACTION_SIP_EVENT_UNREGISTERED);
		filter.addAction(ACTION_SIP_EVENT_REGISTERED);
		filter.addAction(ACTION_SIP_EVENT_ON_INVITE);

		filter.addAction(ACTION_EVENT_ON_FAILURE);
		filter.addAction(ACTION_EVENT_ON_CONNECTED);
		filter.addAction(ACTION_EVENT_ON_DISCONNECTED);
		filter.addAction(ACTION_EVENT_ON_IDLE);
		filter.addAction(ACTION_EVENT_ON_ANSWER);
		filter.addAction(ACTION_EVENT_ON_PROCEEDING);
		filter.addAction(ACTION_EVENT_ON_REDIRECTED);
		
		filter.addCategory(context.getPackageName());
		return filter;
	}
}
