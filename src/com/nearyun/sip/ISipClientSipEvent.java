package com.nearyun.sip;

public interface ISipClientSipEvent {

	void onRegistered();

	void onUnRegistered();

	/**
	 * 被叫时，回调，此时弹出待接听界面，并调用ring()告诉对方已开始响铃
	 * 
	 * @param callerURI
	 * @param callerID
	 */
	void onInvite(int index, String callerURI, String callerID, String callerName);
}
